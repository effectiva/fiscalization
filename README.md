# Fiscalization


## Installation

Add this line to your application's Gemfile:

    gem 'fiscalization'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install fiscalization

## Usage


### Default settings

    FISCALIZATION = Fiscalization::Main.new({
      :certificate_ca_path => "#{Rails.root}/certs/#{RAILS_ENV}/cert.pem",
      :certificate_path => "#{Rails.root}/certs/#{RAILS_ENV}/fiskal_1.pfx",
      :certificate_password => 'secret',
      :oib => 11111111111,
      :u_sust_pdv => true,
      :ozn_slijed => 'P',
      :oib_dev => 85511139770
    })


### Echo

  Options:

    :message // required
  
  Get response from CIS for Echo request:

    FISCALIZATION.response :echo,
                           :message => "testing"

  Returns same message:

    {:code => "200", :message => "testing"}


### OfficeSpace


  Options:

    :id_poruke              // required < Main
    :datum_vrijeme          // required
    :oib                    // required < Main
    :ozn_posl_prostora      // required
    :adresa                 // optional
    :ostali_tipovi_pp       // optional
    :radno_vrijeme          // required
    :datum_pocetka_primjene // required
    :oznaka_zatvaranja      // optional
    :spec_namj              // required < Main (developer's OIB)

  Get response from CIS for OfficeSpace request:

    FISCALIZATION.response :office_space,
                           :datum_vrijeme => Time.now,
                           :ozn_posl_prostora => 1,
                           :adresa => {
                             :ulica => 'Ulica',
                             :kucni_broj => '1',
                             :broj_poste => '10000',
                             :naselje => 'Zagreb',
                             :opcina => 'Zagreb'  
                           },
                           :radno_vrijeme => 'Pon-Pet: 09:00-17:00',
                           :datum_pocetka_primjene => Time.now

  Returns message 'OK' for successful response:

    {:code => "200", :message => "OK"}


### Invoice


  Options:

    :storniraj          // optional
    :certificate        // required < Main
    :id_poruke          // required < Main
    :datum_vrijeme      // required
    :oib                // required < Main
    :u_sust_pdv         // required < Main
    :dat_vrijeme        // required
    :ozn_slijed         // required < Main
    :br_ozn_rac         // required
    :ozn_pos_pr         // required
    :ozn_nap_ur         // required
    :pdv_porezi         // optional
    :pnp_porezi         // optional
    :ostali_porezi      // optional
    :iznos_oslob_pdv    // optional 
    :iznos_marza        // optional
    :iznos_ne_podl_opor // optional
    :naknade            // optional
    :iznos_ukupno       // required
    :nacin_plac         // required 
    :oib_oper           // required
    :nak_dost           // required 
    :paragon_br_rac     // optional
    :spec_namj          // optional


  Get response from CIS for Invoice request:

    FISCALIZATION.response :invoice,
                           :datum_vrijeme => Time.now,
                           :dat_vrijeme => Time.now, 
                           :br_ozn_rac => 1,
                           :ozn_pos_pr => 1,
                           :ozn_nap_ur => 1,
                           :iznos_ukupno => 12.5,
                           :pdv_porezi => [{:stopa => 25, :osnovica => 10.0, :iznos => 2.5}],
                           :nacin_plac => 'K',
                           :oib_oper => 11111111111,
                           :nak_dost => false

  Returns JIR number:

    {:code => "200", :message => "ff455e72-bace-44b8-9e42-6623686219e7"}

  

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
