# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fiscalization/version'

Gem::Specification.new do |spec|
  spec.name          = "fiscalization"
  spec.version       = Fiscalization::VERSION
  spec.authors       = ["Marko Žabčić", "Zoran Žabčić"]
  spec.email         = ["info@effectiva.hr"]
  spec.description   = %q{Fiskalizacija}
  spec.summary       = %q{Fiskalizacija}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 1.8.7'

  spec.add_runtime_dependency 'nokogiri'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end

