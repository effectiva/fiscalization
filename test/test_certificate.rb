require 'test/test_helper'

class CertificateTest < Test::Unit::TestCase


  include TestHelper


  def test_get_key_and_cert
    assert_equal "OpenSSL::PKey::RSA", certificate.key.class.name
    assert_equal "OpenSSL::X509::Certificate", certificate.cert.class.name
  end


  def test_parsed_issuer
    issuer = {"O"=>"FINA", "C"=>"HR", "OU"=>"DEMO"}
    assert_equal issuer, certificate.parsed_issuer
  end


  def test_issuer_for_xml
    issuer = "OU=DEMO,O=FINA,C=HR"
    assert_equal issuer, certificate.issuer_for_xml 
  end


  private


    def certificate
      Fiscalization::Certificate.new 'test/data/fiskal_1.pfx', 'Ut7htw8shM3y'
    end


end


  