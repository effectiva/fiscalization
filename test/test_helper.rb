require 'test/unit'
require 'fiscalization'

module TestHelper


  private 


  	def fiscalization options={}
      Fiscalization::Main.new({
        :certificate_ca_path => 'test/data/democacert.pem',
        :certificate_path => 'test/data/fiskal_1.pfx',
        :certificate_password => 'Ut7htw8shM3y',
        :oib => 85511139770,
        :u_sust_pdv => true,
        :ozn_slijed => 'P',
        :oib_dev => 85511139770
      }.merge(options)) 
    end


    def invoice_options options={}
      { :id_poruke => '622f8404-7de6-4bc6-bddb-06f726a14dff',
        :datum_vrijeme => '24.10.2013T12:13:23',
        :dat_vrijeme => '24.10.2013T12:13:23',
        :br_ozn_rac => 1,
        :ozn_pos_pr => 1,
        :ozn_nap_ur => 1,
        :iznos_ukupno => 125.0,
        :pdv_porezi => [{:stopa => 25, :osnovica => 100.0, :iznos => 25.0}],
        :nacin_plac => 'K',
        :oib_oper => 85511139770,
        :nak_dost => false }.merge(options)
    end


    def invoice_options_2 options={}
      { :datum_vrijeme => Time.now,
        :dat_vrijeme => Time.now,
        :br_ozn_rac => 2,
        :ozn_pos_pr => 1,
        :ozn_nap_ur => 1,
        :iznos_ukupno => 125.0,
        :pdv_porezi => [{:stopa => 25, :osnovica => 100.0, :iznos => 25.0}],
        :nacin_plac => 'K',
        :oib_oper => 85511139770,
        :nak_dost => false }.merge(options)
    end


    def office_space_options options={}
      { :datum_vrijeme => Time.now,
        :ozn_posl_prostora => 1,
        :adresa => {
          :ulica => 'Dragutina Mitica',
          :kucni_broj => '9',
          :broj_poste => '10361',
          :naselje => 'Sesvetski Kraljevec',
          :opcina => 'Sesvete'  
        },
        :radno_vrijeme => 'Pon-Pet: 09:00-17:00',
        :datum_pocetka_primjene => Time.now }.merge(options)
    end


end
