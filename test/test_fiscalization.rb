require 'test/test_helper'

class FiscalizationTest < Test::Unit::TestCase


  include TestHelper


  def test_enviroment
    assert_equal "https://cis.porezna-uprava.hr:8449/FiskalizacijaService", fiscalization(:production => true).cis_url
    assert_equal "https://cistest.apis-it.hr:8449/FiskalizacijaServiceTest", fiscalization.cis_url
  end


  def test_invoice_nokogiri
    sample = File.read('test/data/invoice_xml_sample.xml')
    assert_equal sample, fiscalization.invoice(invoice_options).nokogiri.doc.root.to_xml(:indent => 1)
  end


  def test_echo_request
    response_info(:echo, {:message => "testiranje"})
  end


  def test_invoice_request
    response_info(:invoice, invoice_options)
  end


  def test_invoice_request_2
    response_info(:invoice, invoice_options_2)
  end


  def test_invoice_request_3
    response_info(:invoice, invoice_options_2(
      :pnp_porezi => [{:stopa => 10, :osnovica => 100, :iznos => 10}],
      :ostali_porezi => [{:naziv => 'Porez na luksuz', :stopa => 15, :osnovica => 100, :iznos => 15}],
      :iznos_ukupno => 125 + 110 + 115
    ))
  end


  def test_invoice_request_4
    response_info(:invoice, invoice_options_2(
      :pnp_porezi => [{:stopa => 10, :osnovica => 100, :iznos => 10}],
      :ostali_porezi => [{:naziv => 'Porez na luksuz', :stopa => 15, :osnovica => 100, :iznos => 15},
                         {:naziv => 'Porez na zrak', :stopa => 5, :osnovica => 100, :iznos => 105}],
      :iznos_ukupno => 125 + 110 + 115 + 105
    ))
  end


  def test_office_space_request
    response_info(:office_space, office_space_options)
  end


  def test_echo_response
    response = fiscalization.response(:echo, {:message => "testing echo response"})
    assert_equal "200", response[:code]
    assert_equal "testing echo response", response[:message]
  end


  def test_invoice_error_response
    response = fiscalization.response(:invoice, invoice_options(:datum_vrijeme => "12:00"))
    assert_equal "500", response[:code]
    assert response[:message].include?("DatumVrijeme value '12:00' is not a valid instance of type")
  end


  def test_office_space_error_response
    response = fiscalization.response(:office_space, office_space_options(:adresa => {:opcina => ""}))
    assert_equal "500", response[:code]
    assert_equal "Neispravan digitalni potpis.", response[:message]
  end


  def test_storniraj
    response_info(:invoice, invoice_options_2(
      :pnp_porezi => [{:stopa => 10, :osnovica => 100, :iznos => 10}],
      :ostali_porezi => [{:naziv => 'Porez minus', :stopa => 15, :osnovica => -100, :iznos => -15},
                         {:naziv => 'Porez plus', :stopa => 5, :osnovica => 100, :iznos =>105}],
      :iznos_ukupno => 125 + 110 + 115 + 105,
      :storniraj => true
    ))
  end


  private


    def response_info(type, options)
      response = fiscalization.request(type, options)

      puts "\n" 
      puts "*" * 30
      puts type.to_s
      puts "========== REQUEST BODY ==========" 
      puts fiscalization.xml(type, options)
      puts "========== RESPONSE CODE ==========" 
      puts response.code
      puts "========== RESPONSE BODY ==========" 
      puts response.body
      puts "*" * 30
      puts "\n" 

      assert_equal "200", response.code
    end





end