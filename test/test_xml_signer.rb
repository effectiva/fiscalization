require 'test/test_helper'

class XmlSignerTest < Test::Unit::TestCase


  include TestHelper


  def test_digest_value
    sample = "Y7wJOhrw191zvaGI3O5/AHIEblk="
    assert_equal sample, signer.digest_value(xml_for_signing)
  end


  def test_signature_value
    sample = File.read('test/data/invoice_signature_value_sample.txt')
    assert_equal sample, signer.signature_value(signer.signed_info(xml_for_signing))
  end


  def test_signed_info
    sample = File.read('test/data/invoice_signed_info_sample.xml')
    assert_equal sample, signer.signed_info(xml_for_signing)
  end


  def test_signature
    sample = File.read('test/data/invoice_signature_sample.xml')
    assert_equal  sample, signer.signature(xml_for_signing)
  end


  private


    def signer
      Fiscalization::XmlSigner.new(Fiscalization::Certificate.new('test/data/fiskal_1.pfx', 'Ut7htw8shM3y'))
    end


    def xml_for_signing
      fiscalization.invoice(invoice_options).nokogiri.doc.root
    end


end


  