module Fiscalization
  class OfficeSpace


    attr_accessor :ostali_tipovi_pp,
                  :spec_namj

    attr_reader :id_poruke,
                :datum_vrijeme,
                :oib,
                :ozn_posl_prostora,
                :adresa,
                :ostali_tipovi_pp,
                :radno_vrijeme,
                :datum_pocetka_primjene,
                :oznaka_zatvaranja


    def initialize(args)
      self.id_poruke              = args[:id_poruke]                # < Main
      self.datum_vrijeme          = args[:datum_vrijeme]
      self.oib                    = args[:oib]                      # < Main
      self.ozn_posl_prostora      = args[:ozn_posl_prostora]
      self.adresa                 = args[:adresa]                   if args[:adresa]
      self.ostali_tipovi_pp       = args[:ostali_tipovi_pp]         if args[:ostali_tipovi_pp]
      self.radno_vrijeme          = args[:radno_vrijeme]   
      self.datum_pocetka_primjene = args[:datum_pocetka_primjene]
      self.oznaka_zatvaranja      = args[:oznaka_zatvaranja]        if args[:oznaka_zatvaranja]
      self.spec_namj              = args[:spec_namj]
    end


    # Identifikator poruke 
    #
    # ID poruke (UUID). 
    # Svaka poruka koja se šalje prema CIS-u 
    # mora sadržavati različiti ID poruke. Isto 
    # vrijedi i u slučaju kad se ponavlja slanje 
    # poruke zbog greške u razmjeni poruka. 
    #
    # Fiscalization::Main#uuid
    def id_poruke=(id_poruke)
      raise("Missing attribute: id_poruke") unless id_poruke
      @id_poruke = id_poruke
    end


    # Datum i vrijeme slanja 
    #
    # Datum i vrijeme slanja poruke zahtjeva. 
    # dd.mm.ggggThh:mm:ss 
    def datum_vrijeme=(datum_vrijeme)
      raise("Missing attribute: datum_vrijeme") unless datum_vrijeme
      @datum_vrijeme = datum_vrijeme.is_a?(String) ? datum_vrijeme : datum_vrijeme.strftime("%d.%m.%YT%T")
    end


    # OIB
    #
    # OIB obveznika fiskalizacije. 
    def oib=(oib)
      raise("Missing attribute: oib") unless oib
      @oib = oib
    end


    # Oznaka poslovnog prostora 
    #
    # Može sadržavati samo znamenke i slova 0- 9, a-z, A-Z. 
    # Oznaka mora biti ista kao ona koja se navodi na računima. 
    # Mora biti jedinstvena na razini OIB-a obveznika. 
    def ozn_posl_prostora=(ozn_posl_prostora)
      raise("Missing attribute: ozn_posl_prostora") unless ozn_posl_prostora
      @ozn_posl_prostora = ozn_posl_prostora
    end


    # Specifična namjena 
    #
    # Potrebno je dostaviti jedan od podataka u nastavku: 
    #   * OIB pravne ili fizičke osobe koja je proizvela programsko rješenje ili 
    #   * OIB pravne ili fizičke osobe koja održava programsko rješenje ili 
    #   * OIB pravne ili fizičke osobe prodavatelja u slučaju da se koristi rješenje od stranog proizvođača – bez lokalnog partnera 


    # Adresni podatak 
    #
    # Adresa ili opis tipa poslovnog prostora (npr. Internet trgovina) kada poslovni prostor nema adresu. 
    # Mora se dostaviti jedan od ta dva podatka.
    #
    #   Adresa
    #
    #   Adresa poslovnog prostora. 
    #   Podatak se sastoji od ulice, kućnog broja, dodatka kućnom broju, broja pošte, naselja i općine.
    #
    #   Ostali tipovi poslovnog prostora   
    #
    #   Predviđen je slobodan unos za specifične slučajeve kada ne postoji adresa poslovnog prostora (Internet trgovina, pokretna trgovina i sl). 
    def adresa=(adresa)
      raise("Invalid type: adresa must be hash") unless adresa.is_a?(Hash)
      @adresa = adresa
    end


    # Radno vrijeme 
    #
    # Radno vrijeme poslovnog prostora. 
    # Predviđen je slobodan unos. 
    def radno_vrijeme=(radno_vrijeme)
      raise("Missing attribute: radno_vrijeme") unless radno_vrijeme
      @radno_vrijeme = radno_vrijeme
    end
 
 
    # Datum početka primjene 
    #
    # Datum otkad vrijedi promjena. 
    # dd.mm.gggg 
    def datum_pocetka_primjene=(datum_pocetka_primjene)
      raise("Missing attribute: datum_pocetka_primjene") unless datum_pocetka_primjene
      @datum_pocetka_primjene = datum_pocetka_primjene.is_a?(String) ? datum_pocetka_primjene : datum_pocetka_primjene.strftime("%d.%m.%Y")
    end


    # Oznaka zatvaranja 
    #
    # Podatak se dostavlja Poreznoj upravi samo ako se trajno zatvara poslovni prostor. 
    # U tom se slučaju dostavlja zadnje radno vrijeme i adresa ili opis tipa poslovnog 
    # prostora (jer su obavezni podaci u poruci). 
    # Moguća vrijednost je „Z“. 
    # Nakon zatvaranja poslovnog prostora ne 
    # smiju se više dostavljati računi s oznakom 
    # tog poslovnog prostora. 
    def oznaka_zatvaranja=(oznaka_zatvaranja)
      raise("Invalid value: oznaka_zatvaranja => #{oznaka_zatvaranja}") unless oznaka_zatvaranja == 'Z'
      @oznaka_zatvaranja = oznaka_zatvaranja
    end



    def nokogiri(signature=false)
      Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|

        xml['tns'].PoslovniProstorZahtjev('xmlns:tns' => 'http://www.apis-it.hr/fin/2012/types/f73',
                                'Id' => 'za_potpisivanje'){

          xml.Zaglavlje {
            xml.IdPoruke id_poruke
            xml.DatumVrijeme datum_vrijeme
          }

          xml.PoslovniProstor {
            xml.Oib oib
            xml.OznPoslProstora ozn_posl_prostora

            xml.AdresniPodatak {
              if adresa
                xml.Adresa {
                  xml.Ulica adresa[:ulica] if adresa[:ulica]
                  xml.KucniBroj adresa[:kucni_broj] if adresa[:kucni_broj]
                  xml.KucniBrojDodatak adresa[:kucni_broj_dodatak] if adresa[:kucni_broj_dodatak]
                  xml.BrojPoste adresa[:broj_poste] if adresa[:broj_poste]
                  xml.Naselje adresa[:naselje] if adresa[:naselje]
                  xml.Opcina adresa[:opcina] if adresa[:opcina]
                }
              end
              xml.OstaliTipoviPP ostali_tipovi_pp if ostali_tipovi_pp
            }
            
            xml.RadnoVrijeme radno_vrijeme
            xml.DatumPocetkaPrimjene datum_pocetka_primjene
            xml.OznakaZatvaranja 'Z' if oznaka_zatvaranja
            xml.SpecNamj spec_namj
          }
          xml.Signature 'SIGNATURE' if signature
        }
      end
    end


  end
end