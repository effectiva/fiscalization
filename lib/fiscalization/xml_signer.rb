module Fiscalization
  class XmlSigner

    
    attr_reader :certificate


    def initialize(certificate)
      @certificate = certificate
    end


    def digest_value(xml_for_signing)
      Base64.encode64(OpenSSL::Digest::SHA1.digest(xml_for_signing.to_xml(:indent => 1))).gsub(/\n/, '')
    end


    def signature_value(xml_for_signing)
      certificate.sign_base64(xml_for_signing).gsub(/\n/, '')
    end


    def signed_info(xml_for_signing)
      Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.SignedInfo('xmlns' => 'http://www.w3.org/2000/09/xmldsig#'){
          xml.CanonicalizationMethod('Algorithm' => 'http://www.w3.org/2001/10/xml-exc-c14n#')
          xml.SignatureMethod('Algorithm' => 'http://www.w3.org/2000/09/xmldsig#rsa-sha1')
          xml.Reference('URI' => '#za_potpisivanje'){
            xml.Transforms{
              xml.Transform('Algorithm' => 'http://www.w3.org/2000/09/xmldsig#enveloped-signature')
              xml.Transform('Algorithm' => 'http://www.w3.org/2001/10/xml-exc-c14n#')
            }
            xml.DigestMethod('Algorithm' => 'http://www.w3.org/2000/09/xmldsig#sha1')
            xml.DigestValue digest_value(xml_for_signing)
          }
        }
      end.doc.root.canonicalize(Nokogiri::XML::XML_C14N_EXCLUSIVE_1_0)     
    end


    def signature(xml_for_signing)
      Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.Signature('xmlns' => 'http://www.w3.org/2000/09/xmldsig#'){
          xml << signed_info(xml_for_signing)
          xml.SignatureValue signature_value(signed_info(xml_for_signing))
          xml.KeyInfo{
            xml.X509Data{
              xml.X509Certificate certificate.pem_raw
              xml.X509IssuerSerial {
                xml.X509IssuerName certificate.issuer_for_xml
                xml.X509SerialNumber certificate.cert.serial
              }
            }
          }
        }
      end.doc.root.canonicalize(Nokogiri::XML::XML_C14N_EXCLUSIVE_1_0)
    end


  end
end