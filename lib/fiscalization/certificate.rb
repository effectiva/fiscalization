module Fiscalization
  class Certificate


    def initialize(path, password)
      @p12 = OpenSSL::PKCS12.new(File.read(path), password)
    end


    def cert
      @p12.certificate
    end


    def key
      @p12.key
    end


    def parsed_issuer
      cert.issuer.to_a.inject({}){|h, item| h.update({item[0] => item[1]})}
    end


    def issuer_for_xml
      "OU=#{parsed_issuer['OU']},O=#{parsed_issuer['O']},C=#{parsed_issuer['C']}"
    end


    def sign(string)
      key.sign(OpenSSL::Digest::SHA1.new, string)
    end


    def sign_base64(string)
      Base64.encode64 sign(string)
    end


    def pem_raw
      cert.to_pem.sub('-----BEGIN CERTIFICATE-----', '').sub('-----END CERTIFICATE-----', '')
    end


  end
end