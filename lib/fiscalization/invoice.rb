  module Fiscalization
	  class Invoice


	  	attr_accessor :nak_dost,
	                  :paragon_br_rac,
	                  :spec_namj,
	                  :storniraj


	  	attr_reader :certificate,
	  		          :id_poruke,
	    	          :datum_vrijeme,
	    	          :oib,
	                :u_sust_pdv,
	                :dat_vrijeme,
	                :ozn_slijed,
	    	          :br_ozn_rac,
	    	          :ozn_pos_pr,
	    	          :ozn_nap_ur,
	                :pdv_porezi,
	                :pnp_porezi,
	                :ostali_porezi,
	                :iznos_oslob_pdv,
	                :iznos_marza,
	                :iznos_ne_podl_opor,
	                :naknade,
	                :iznos_ukupno,
	                :nacin_plac,
	                :oib_oper
	

	    def initialize(args)
	      self.storniraj          = args[:storniraj]          if args[:storniraj]

	    	self.certificate        = args[:certificate]        # < Main
	  		self.id_poruke          = args[:id_poruke]          # < Main
	    	self.datum_vrijeme      = args[:datum_vrijeme]
	    	self.oib                = args[:oib]                # < Main
	      self.u_sust_pdv         = args[:u_sust_pdv]         # < Main
	      self.dat_vrijeme        = args[:dat_vrijeme]
	      self.ozn_slijed         = args[:ozn_slijed]         # < Main
	    	self.br_ozn_rac         = args[:br_ozn_rac]
	    	self.ozn_pos_pr         = args[:ozn_pos_pr]
	    	self.ozn_nap_ur         = args[:ozn_nap_ur]
	      self.pdv_porezi         = args[:pdv_porezi]         if args[:pdv_porezi]
	      self.pnp_porezi         = args[:pnp_porezi]         if args[:pnp_porezi]
	      self.ostali_porezi      = args[:ostali_porezi]      if args[:ostali_porezi]
	      self.iznos_oslob_pdv    = args[:iznos_oslob_pdv]    if args[:iznos_oslob_pdv] 
	      self.iznos_marza        = args[:iznos_marza]        if args[:iznos_marza]
	      self.iznos_ne_podl_opor = args[:iznos_ne_podl_opor] if args[:iznos_ne_podl_opor]
	      self.naknade            = args[:naknade]            if args[:naknade]
	      self.iznos_ukupno       = args[:iznos_ukupno]
	      self.nacin_plac         = args[:nacin_plac]
	      self.oib_oper           = args[:oib_oper]
	      self.nak_dost           = args[:nak_dost] 
	      self.paragon_br_rac     = args[:paragon_br_rac]     if args[:paragon_br_rac]
	      self.spec_namj          = args[:spec_namj]          if args[:spec_namj]
	    end


      def certificate=(certificate)
      	raise("Missing attribute: certificate") unless certificate
      	@certificate = certificate
      end

      
      # Identifikator poruke 
      #
			# ID poruke (UUID). 
			# Svaka poruka koja se šalje prema CIS-u 
			# mora sadržavati različiti ID poruke. Isto 
			# vrijedi i u slučaju kad se ponavlja slanje 
			# poruke zbog greške u razmjeni poruka. 
      #
      # Fiscalization::Main#uuid
      def id_poruke=(id_poruke)
      	raise("Missing attribute: id_poruke") unless id_poruke
      	@id_poruke = id_poruke
      end

      # Datum i vrijeme slanja 
      #
			# Datum i vrijeme slanja poruke zahtjeva. 
			# dd.mm.ggggThh:mm:ss 
	    def datum_vrijeme=(datum_vrijeme)
	    	raise("Missing attribute: datum_vrijeme") unless datum_vrijeme
	    	@datum_vrijeme = datum_vrijeme.is_a?(String) ? datum_vrijeme : datum_vrijeme.strftime("%d.%m.%YT%T")
	    end

      
      # OIB
      #
      # OIB obveznika fiskalizacije. 
	    def oib=(oib)
	    	raise("Missing attribute: oib") unless oib
	    	@oib = oib
	    end

      
      # U sustavu PDV
      #
      # Oznaka je li obveznik u sustavu PDV-a ili nije. 
      # True ako je obveznik u sustavu PDV-a, u suprotnom false. 
      # Boolean (1-true/0-false) 
	    def u_sust_pdv=(u_sust_pdv)
	    	raise("Missing attribute: u_sust_pdv") if u_sust_pdv.nil?
	    	@u_sust_pdv = u_sust_pdv
	    end


      # Datum i vrijeme izdavanja 
      #
			# Datum i vrijeme izdavanja koji se ispisuju na računu. 
			# dd.mm.ggggThh:mm:ss 
	    def dat_vrijeme=(dat_vrijeme)
	    	raise("Missing attribute: dat_vrijeme") unless dat_vrijeme
	    	@dat_vrijeme = dat_vrijeme.is_a?(String) ? dat_vrijeme : dat_vrijeme.strftime("%d.%m.%YT%T")
	    end

     
      # Oznaka slijednosti
      #
			# Oznaka slijednosti brojeva računa. 
			# Oznaka koja govori gdje se određuje 
			# dodjela broja računa tj. dodjeljuje li se broj 
			# računa centralno na razini poslovnog 
			# prostora ili pojedinačno na svakom 
			# naplatnom uređaju. 
			# Dodjela brojeva računa može biti na razini 
			# poslovnog prostora ili naplatnog uređaja: 
			#
			#   P - na nivou poslovnog prostora 
			#   N - na nivou naplatnog uređaja 
	    def ozn_slijed=(ozn_slijed)
	    	raise("Missing attribute: ozn_slijed") unless ozn_slijed
	    	raise("Invalid value: ozn_slijed => #{ozn_slijed}") unless %w(P N).include?(ozn_slijed)
	    	@ozn_slijed = ozn_slijed
	    end


      # Broj računa 
      #
			# Propisati će se izgled broja računa kako se 
			# treba ispisivati na fizičkom računu u sljedećem obliku: 
			#
			#   brojčana oznaka računa/oznaka poslovnog prostora/oznaka naplatnog uređaja
			#   Primjer: 1234567890/POSL1/12
			#
			# Podaci će se Poreznoj upravi dostavljati
			# odvojeni u XML-u radi kasnije lakše
			# manipulacije u izvještajima i analizama.

      
      # Brojčana oznaka računa
      #
      # Može sadržavati samo znamenke 0-9.
      # Nisu dozvoljene vodeće nule. 
      def br_ozn_rac=(br_ozn_rac)
      	raise("Missing attribute: br_ozn_rac") unless br_ozn_rac
      	@br_ozn_rac = br_ozn_rac
      end


      # Oznaka poslovnog prostora 
      #
			# Može sadržavati samo znamenke i slova 0-9, a-z, A-Z. 
			# Mora biti jedinstvena na razini OIB-a obveznika. 
      def ozn_pos_pr=(ozn_pos_pr)
      	raise("Missing attribute: ozn_pos_pr") unless ozn_pos_pr
      	@ozn_pos_pr = ozn_pos_pr
      end


      # Oznaka naplatnog uređaja
      #
			# Može sadržavati samo znamenke 0-9. 
			# Nisu dozvoljene vodeće nule. 
			# Mora biti jedinstvena na razini jednog 
			# poslovnog prostora obveznika. 
      def ozn_nap_ur=(ozn_nap_ur)
      	raise("Missing attribute: ozn_nap_ur") unless ozn_nap_ur
      	@ozn_nap_ur = ozn_nap_ur
      end


      # PDV
      #
			# Porez na dodanu vrijednost.
			# Podatak se dostavlja Poreznoj upravi samo ako na računu postoji PDV.
			# Podatak se sastoji od porezne stope, osnovice i iznosa poreza.
			# Može postojati lista poreznih stopa.
			#
			#   Porezna stopa 
      #   Iznos porezne stope. Npr. stopa od 25,00% se dostavlja kao 25.00. 
      #
      #   Osnovica 
      #   Iznos osnovice. 
      #
      #   Iznos poreza   
      def pdv_porezi=(pdv_porezi)
      	raise("Invalid type: pdv_porezi must be array") unless pdv_porezi.is_a?(Array)
      	pdv_porezi.each do |porez|
          porez[:stopa]    = formated porez[:stopa]
          porez[:osnovica] = formated porez[:osnovica], storniraj
          porez[:iznos]    = formated porez[:iznos], storniraj 
      	end
      	@pdv_porezi = pdv_porezi
      end

      
      # PNP
      #
			# Porez na potrošnju. 
			# Podatak se dostavlja Poreznoj upravi samo ako na računu postoji porez na potrošnju. 
			# Podatak se sastoji od porezne stope, osnovice i iznosa poreza. 
			# Može postojati lista poreznih stopa. 
			#
			#   Porezna stopa 
      #   Iznos porezne stope. Npr. stopa od 3,00% se dostavlja kao 3.00.
      #
      #   Osnovica 
      #   Iznos osnovice. 
      #
      #   Iznos poreza   
      def pnp_porezi=(pnp_porezi)
      	raise("Invalid type: pnp_porezi must be array") unless pnp_porezi.is_a?(Array)
      	pnp_porezi.map do |porez|
          porez[:stopa]    = formated porez[:stopa]
          porez[:osnovica] = formated porez[:osnovica], storniraj
          porez[:iznos]    = formated porez[:iznos], storniraj
      	end
      	@pnp_porezi = pnp_porezi
      end

      
      # Ostali porezi
      #
			# Ostali porezi koji se mogu pojaviti na računu osim PDV-a i poreza na potrošnju.
			# Podatak se dostavlja Poreznoj upravi samo ako na računu postoje ostali porezi.
			# Podatak se sastoji od naziva poreza, porezne stope, osnovice i iznosa poreza.
			# Može postojati lista poreza.
			#
			#   Naziv poreza 
			#   Naziv poreza za koji se šalju podaci. Npr. Porez na luksuz.
			#
			#   Porezna stopa 
      #   Iznos porezne stope. Npr. stopa od 15,00% se dostavlja kao 15.00.
      #
      #   Osnovica 
      #   Iznos osnovice. 
      #
      #   Iznos poreza 
      def ostali_porezi=(ostali_porezi)
      	raise("Invalid type: ostali_porezi must be array") unless ostali_porezi.is_a?(Array)
      	ostali_porezi.map do |porez|
         #porez[:naziv]    = porez[:naziv] 
          porez[:stopa]    = formated porez[:stopa] 
          porez[:osnovica] = formated porez[:osnovica], storniraj 
          porez[:iznos]    = formated porez[:iznos], storniraj
      	end
      	@ostali_porezi = ostali_porezi
      end


			# Iznos oslobođenja 
			#
			# Ukupni iznos oslobođenja na računu. 
			# Oslobođenja u slučajevima kada se isporučuju dobra ili obavljaju usluge koje su oslobođene od plaćanja PDV-a. 
			# Podatak se dostavlja Poreznoj upravi samo ako na računu postoji oslobođenje. 
      def iznos_oslob_pdv=(iznos_oslob_pdv)
      	@iznos_oslob_pdv = formated iznos_oslob_pdv, storniraj
      end

      # Iznos na koji se odnosi poseban postupak oporezivanja marže 
      #
      # Ukupni iznos na koji se odnosi poseban postupak oporezivanja marže na računu. 
      # Marža za rabljena dobra, umjetnička djela, kolekcionarske ili antikne predmete (članak 22.a Zakona o PDV-u).
      # Podatak se dostavlja Poreznoj upravi samo ako na računu postoji poseban postupak oporezivanja marže.
      def iznos_marza=(iznos_marza)
      	@iznos_marza = formated iznos_marza, storniraj
      end


      # Iznos koji ne podliježe oporezivanju 
      #
      # Ukupni iznos koji ne podliježe oporezivanju na računu. 
      # Podatak se dostavlja Poreznoj upravi samo ako na računu postoji iznos koji ne podliježe oporezivanju.
      def iznos_ne_podl_opor=(iznos_ne_podl_opor)
        @iznos_ne_podl_opor = formated iznos_ne_podl_opor, storniraj
      end

      
      # Naknade 
      #
      # Naknade koje se mogu pojaviti na računu tipa povratne naknade za ambalažu i sl. 
      # Podatak se dostavlja Poreznoj upravi samo ako na računu postoje naknade. 
      # Podatak se sastoji od naziva naknade i iznosa naknade. 
      # Može postojati lista naknada. 
      def naknade=(naknade)
      	raise("Invalid type: naknade must be array") unless naknade.is_a?(Array)
      	naknade.map do |naknada|
          naknada[:naziv_n] = naknada[:naziv_n] 
          naknada[:iznos_n] = formated naknada[:iznos_n], storniraj
      	end
      	@naknade = naknade
      end


      # Ukupan iznos 
      #
      # Ukupan iznos iskazan na računu. 
      def iznos_ukupno=(iznos_ukupno)
        @iznos_ukupno = formated iznos_ukupno, storniraj
      end


      # Način plaćanja 
      #
	    #  G – gotovina 
			#  K – kartice 
			#  C – ček 
			#  T – transakcijski račun 
			#  O – ostalo 
			#
			#  U slučaju više načina plaćanja po jednom računu, isto je potrebno prijaviti pod 'Ostalo'. 
			#  Za sve načine plaćanja koji nisu prije navedeni koristiti će se oznaka ‘Ostalo’. 
	    def nacin_plac=(nacin_plac)
	    	raise("Missing attribute: nacin_plac") if nacin_plac.nil?
	    	raise("Invalid value: nacin_plac => #{nacin_plac}") unless  %w(G K C T O).include?(nacin_plac)
        @nacin_plac = nacin_plac
	    end


      # OIB operatera na naplatnom uređaju 
      #
			# OIB operatera na naplatnom uređaju koji izdaje račun.
			# U slučaju samouslužnih naplatnih uređaja i automata potrebno je dostaviti OIB izdavatelja (OIB s računa). 
	    def oib_oper=(oib_oper)
	    	raise("Missing attribute: oib_oper") if oib_oper.nil?
        @oib_oper = oib_oper
	    end


      # Zaštitni kod izdavatelja
      #
      # Zaštitni kod izdavatelja obveznika fiskalizacije je alfanumerički zapis kojim se potvrđuje veza između obveznika fiskalizacije i izdanog računa. 
      # 32-znamenkasti broj zapisan u heksadecimalnom formatu. 
      # Može sadržavati znamenke 0-9 i mala slova a-f. 
      #
      # Primjer: 
      # e4d909c290d0fb1ca068ffaddf22cbd0 
      #
	    # Algoritam za izračun zaštitnog koda može se sažeto prikazati na sljedeći način:
	    # MD5 hash (Elektronički potpis privatnim ključem (OIB+datum i vrijeme izdavanja+brojčana oznaka
	    # računa+oznaka poslovnog prostora+oznaka naplatnog uređaja+ukupni iznos računa))
	    def zast_kod
	      code = [:oib, :datum_vrijeme, :br_ozn_rac, :ozn_pos_pr, :ozn_nap_ur, :iznos_ukupno].map do |attribute|
	        value = self.send(attribute)
	        value = value.to_f.abs if attribute == :iznos_ukupno
	        value = value.to_s # nil.to_s => ""
	        raise("Missing attribute: #{attribute}") if value.empty?
	        value
	      end.join
	      OpenSSL::Digest::MD5.new(certificate.sign(code))
	    end


      # Oznaka naknadne  dostave računa 
      # 
			# Pod naknadnom dostavom računa Poreznoj upravi smatra se situacija kad je isti prethodno izdan kupcu bez JIR-a 
			# (npr. prekid Internet veze ili potpuni prestanak rada naplatnog uređaja). 
			# True ako je riječ o naknadnoj dostavi računa, u suprotnom false. 
			# Boolean (1-true/0-false) 
      #
      # attr_accessor // nije potreban custom setter/getter

    
      # Oznaka paragon računa 
      #
      # Dostavlja se samo u slučaju potpunog prestanka rada naplatnog uređaja kada obveznik fiskalizacije 
      # mora prepisati izdane paragon račune i prijaviti ih putem poruke Poreznoj upravi. 
      #
      # attr_accessor // nije potreban custom setter/getter


      # Specifična namjena 
      #
      # Predviđeno za slučaj da se naknadno pojavi potreba za dostavom podataka koji nisu prepoznati tokom analize. 
      #
      # attr_accessor // nije potreban custom setter/getter


	    def nokogiri(signature=false)
	      Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
	        xml['tns'].RacunZahtjev(
	        	'Id' => 'za_potpisivanje',
	        	'xmlns:tns' => 'http://www.apis-it.hr/fin/2012/types/f73'
	        	#'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
	        	#'xsi:schemaLocation' => 'http://www.apis-it.hr/fin/2012/types/f73 ../schema/FiskalizacijaSchema.xsd',
	        	){
	          
	          # Zaglavlje
	          xml.Zaglavlje {
	            xml.IdPoruke id_poruke
	            xml.DatumVrijeme datum_vrijeme
	          }

	          # Račun
	          xml.Racun {
	            xml.Oib oib
	            xml.USustPdv u_sust_pdv
	            xml.DatVrijeme dat_vrijeme
	            xml.OznSlijed ozn_slijed

	            # Broj računa br-prostor-uređaj
	            xml.BrRac {
	              xml.BrOznRac br_ozn_rac
	              xml.OznPosPr ozn_pos_pr
	              xml.OznNapUr ozn_nap_ur
	            }

	            # PDV
	            if pdv_porezi
		            xml.Pdv {
		              pdv_porezi.each do |porez|
		                xml.Porez {
		                  xml.Stopa porez[:stopa]
		                  xml.Osnovica porez[:osnovica]
		                  xml.Iznos porez[:iznos]
		                }
		              end
		            }
	            end

	            # PNP
	            if pnp_porezi
	              xml.Pnp {
	                pnp_porezi.each do |porez|
	                  xml.Porez {
	                    xml.Stopa porez[:stopa]
	                    xml.Osnovica porez[:osnovica]
	                    xml.Iznos porez[:iznos]
	                  }
	                end
	              }
	            end
              
              # Ostali porezi
	            if ostali_porezi
	              xml.OstaliPor {
	                ostali_porezi.each do |porez|
	                  xml.Porez {
	                    xml.Naziv porez[:naziv]
	                    xml.Stopa porez[:stopa]
	                    xml.Osnovica porez[:osnovica]
	                    xml.Iznos porez[:iznos]
	                  }
	                end
	              }
	            end
              
	            xml.IznosOslobPdv iznos_oslob_pdv if iznos_oslob_pdv
	            xml.IznosMarza iznos_marza if iznos_marza

	            # Naknade
	            if naknade
		            xml.Naknade {
		              naknade.each do |naknada|
		                xml.Naknada {
		                  xml.NazivN porez[:naziv_n]
		                  xml.IznosN porez[:iznos_n]
		                }
		              end
		            }
	            end

	            xml.IznosUkupno iznos_ukupno
	            xml.NacinPlac nacin_plac
	            xml.OibOper oib_oper
	            xml.ZastKod zast_kod
	            xml.NakDost nak_dost
	            xml.SpecNamj spec_namj if spec_namj
	            xml.ParagonBrRac paragon_br_rac if paragon_br_rac
	          }
	          xml.Signature 'SIGNATURE' if signature
	        }
	      end
	    end


	    private


		    def formated(n, s=false)
		    	n *= -1 if s && (n.is_a?(Integer) || n.is_a?(Float))
		    	sprintf("+%.2f", n).sub('+', '')
		    end


	  end
  end