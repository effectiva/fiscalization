require 'rubygems'
require 'nokogiri'

require 'tempfile'
require 'base64'
require 'openssl'
require 'net/https'
require 'securerandom'

require "fiscalization/certificate"
require "fiscalization/xml_signer"
require "fiscalization/invoice"
require "fiscalization/office_space"
require "fiscalization/version"

module Fiscalization


  class Main


    def initialize(args={})
      [:certificate_ca_path, :certificate_path, :certificate_password, :oib, :u_sust_pdv, :ozn_slijed, :oib_dev].each do |arg|
        raise("Missing attribute: #{arg}") unless args[arg]
      end
      
      @certificate_ca_path = args[:certificate_ca_path]
      @certificate    = Fiscalization::Certificate.new(args[:certificate_path], args[:certificate_password])
      @signer         = Fiscalization::XmlSigner.new(@certificate)
      @oib            = args[:oib]
      @u_sust_pdv     = args[:u_sust_pdv]
      @ozn_slijed     = args[:ozn_slijed]
      @oib_dev        = args[:oib_dev]

      @production     = args.fetch(:production, false)
    end


    # Prihvat podataka o računima i poslovnim prostorima
    def cis_url
      if @production
        "https://cis.porezna-uprava.hr:8449/FiskalizacijaService"
      else
        "https://cistest.apis-it.hr:8449/FiskalizacijaServiceTest"
      end
    end


    def response(type, options)
      response = request(type, options)
      nokogiri = Nokogiri::XML(response.body)
      code = response.code
       
      return { :code => 500, :message => 'Invalid response certificate' } unless response_cert_verify(nokogiri, type)

      if code == "200"
        case type
          when :echo
            { :code => code, 
              :message => nokogiri.at_xpath('//tns:EchoResponse', 'tns' => "http://www.apis-it.hr/fin/2012/types/f73").text }
          when :invoice
            { :code => code, 
              :message => nokogiri.at_xpath('//tns:Jir', 'tns' => "http://www.apis-it.hr/fin/2012/types/f73").text }
          when :office_space
            { :code => code, 
              :message => 'OK' }
          else 
            raise("Unknown type: #{type}, allowed: :echo, :invoice, :office_space")
        end
      else
        { :code => code, 
          :message =>  nokogiri.at_xpath('//tns:Greske/tns:Greska/tns:PorukaGreske', 'tns' => "http://www.apis-it.hr/fin/2012/types/f73").text }
      end
    end


    def request(type, options)
      uri = URI.parse(cis_url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      http.ca_file = @certificate_ca_path
      request = Net::HTTP::Post.new(uri.path)
      request.body = xml(type, options)

      http.request(request)
    end


    def xml(type, options)
      return self.send(type, options).to_xml(:indent => 1) if type == :echo
      with_signature(self.send(type, options))
    end


    def echo(options={})
      Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml['soapenv'].Envelope('xmlns:soapenv' => 'http://schemas.xmlsoap.org/soap/envelope/'){
          xml.Body {
            xml['f73'].EchoRequest('xmlns:f73' => 'http://www.apis-it.hr/fin/2012/types/f73'){xml<< options[:message]}
          }
        }
      end
    end


    def invoice(options={})
      Fiscalization::Invoice.new({
        :certificate => @certificate,
        :id_poruke => uuid,
        :oib => @oib,
        :u_sust_pdv => @u_sust_pdv,
        :ozn_slijed => @ozn_slijed
      }.merge(options))
    end


    def office_space(options={})
      Fiscalization::OfficeSpace.new({
        :id_poruke => uuid,
        :oib => @oib,
        :spec_namj => @oib_dev
      }.merge(options))
    end


    private


      def response_cert_verify(nokogiri, type)
        return true if type == :echo

        x509_certificate = nokogiri.at_xpath('//ds:X509Certificate', 'ds' => "http://www.w3.org/2000/09/xmldsig#")
        return false unless x509_certificate 
        
        signed_info = nokogiri.at_xpath('//ds:SignedInfo', 'ds' => "http://www.w3.org/2000/09/xmldsig#")
        signature_value = nokogiri.at_xpath('//ds:SignatureValue', 'ds' => "http://www.w3.org/2000/09/xmldsig#")

        data = signed_info ? signed_info.canonicalize(Nokogiri::XML::XML_C14N_1_0) : ''
        signature = signature_value ? Base64.decode64(signature_value.text) : ''

        b64 = Base64.decode64(x509_certificate.text)
        response_cert = OpenSSL::X509::Certificate.new(b64)
    
        response_cert.public_key.verify(OpenSSL::Digest::SHA1.new, signature, data)
      end


      def with_signature(document)
        signature = @signer.signature(document.nokogiri.doc.root)

        final = document.nokogiri(true).doc.root.to_xml(:indent => 1)
        final = final.sub(" <tns:Signature>SIGNATURE</tns:Signature>", signature)
        final = soap_envelope.sub("BODY", final)
        final = final.sub(/<\/Signature>\n/, '</Signature>')

        final 
      end


      def soap_envelope
        Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml['soapenv'].Envelope('xmlns:soapenv' => 'http://schemas.xmlsoap.org/soap/envelope/'){
            xml.Body "BODY"
          }
        end.doc.to_xml(:indent => 1)
      end

       
      # ID poruke (UUID). 
      # Svaka poruka koja se šalje prema CIS-u 
      # mora sadržavati različiti ID poruke. Isto 
      # vrijedi i u slučaju kad se ponavlja slanje 
      # poruke zbog greške u razmjeni poruka. 
      def uuid
        ary = SecureRandom.random_bytes(16).unpack("NnnnnN")
        ary[2] = (ary[2] & 0x0fff) | 0x4000
        ary[3] = (ary[3] & 0x3fff) | 0x8000
        "%08x-%04x-%04x-%04x-%04x%08x" % ary
      end


  end
end